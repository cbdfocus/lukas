<?php

/**
 * Created by PhpStorm.
 * User: baodong
 * Date: 15/8/13
 * Time: 15:29
 */
class LoadAction extends Action
{
    public function load()
    {

        $uid =  $_GET["_URL_"][2];
        $DAO = M('');
        $re = $DAO->query("
            select * from boygirlgod_main where id =" . $uid
        );

        $hero = $re[0]['hero'];
        $maxv = $re[0]['maxv'];
        $maxs = $re[0]['maxs'];
        $ans = json_decode($re[0]['ans']);

//        echo var_dump($hero);
//        echo var_dump($maxv);
//        echo var_dump($ans);

        $max = $ans[0][0];
        $bg = 'Public/hero.jpg';
        if($hero == 'boy'){
            //男神
            if($maxs == '1颜值'){
                $bg = 'Public/hero/hero-01.jpg';
            }else if($maxs == '2财力'){
                $bg = 'Public/hero/hero-02.jpg';
            }else if($maxs == '3霸道'){
                $bg = 'Public/hero/hero-03.jpg';
            }else if($maxs == '4高冷'){
                $bg = 'Public/hero/hero-04.jpg';
            }else if($maxs == '5体贴'){
                $bg = 'Public/hero/hero-05.jpg';
            }else if($maxs == '6傲娇'){
                $bg = 'Public/hero/hero-06.jpg';
            }else if($maxs == '7呆萌'){
                $bg = 'Public/hero/hero-07.jpg';
            }else if($maxs == '8逗比'){
                $bg = 'Public/hero/hero-08.jpg';
            }
        }else{
            //女神
            if($maxs == '1颜值'){
                $bg = 'Public/hero/heroine-01.jpg';
            }else if($maxs == '2财力'){
                $bg = 'Public/hero/heroine-02.jpg';
            }else if($maxs == '3霸道'){
                $bg = 'Public/hero/heroine-03.jpg';
            }else if($maxs == '4高冷'){
                $bg = 'Public/hero/heroine-04.jpg';
            }else if($maxs == '5体贴'){
                $bg = 'Public/hero/heroine-05.jpg';
            }else if($maxs == '6傲娇'){
                $bg = 'Public/hero/heroine-06.jpg';
            }else if($maxs == '7呆萌'){
                $bg = 'Public/hero/heroine-07.jpg';
            }else if($maxs == '8逗比'){
                $bg = 'Public/hero/heroine-08.jpg';
            }
        }
        //根据路径加载背景英雄图
        $im = imagecreatefromjpeg($bg);


//    定义色条颜色
        $colorLine1 = imagecolorallocate($im, 0xF2, 0xc5, 0x00);
        $colorLine2 = imagecolorallocate($im, 0xF5, 0x9d, 0x00);
        $colorLine3 = imagecolorallocate($im, 0x2c, 0x97, 0xde);
        $colorLine4 = imagecolorallocate($im, 0x1e, 0xca, 0x6b);
        $colorLine5 = imagecolorallocate($im, 0xff, 0x3a, 0x56);
        $wordColor1 = imagecolorallocate($im, 0xff, 0xff, 0xff);
        $wordColor2 = imagecolorallocate($im, 0xf2, 0xc5, 0x00);

//    画色条
        $left_top_x = 400;
        $left_top_y = 580;
        $color_height = 30;
        $gap_height = 20;
        $GAP = $color_height + $gap_height;

        $len100 = 220;
        $len100 /= $maxv;
//        echo $maxv;
//        echo "\n";
//        echo $len100;
//        echo "\n";
//        echo $ans[0][0];
//        exit;

        imagefilledrectangle($im, $left_top_x, $left_top_y, $left_top_x + $len100*$ans[0][0], $left_top_y + $color_height, $colorLine1);
        imagefilledrectangle($im, $left_top_x, $left_top_y + $GAP, $left_top_x + $len100*$ans[1][0], $left_top_y + $color_height + $GAP, $colorLine2);
        imagefilledrectangle($im, $left_top_x, $left_top_y + $GAP * 2, $left_top_x + $len100*$ans[2][0], $left_top_y + $color_height + $GAP * 2, $colorLine3);
        imagefilledrectangle($im, $left_top_x, $left_top_y + $GAP * 3, $left_top_x + $len100*$ans[3][0], $left_top_y + $color_height + $GAP * 3, $colorLine4);
        imagefilledrectangle($im, $left_top_x, $left_top_y + $GAP * 4, $left_top_x + $len100*$ans[4][0], $left_top_y + $color_height + $GAP * 4, $colorLine5);

        imagefttext($im, 22, 0, 270, 584 + 24 + 0, $wordColor1, 'Public/font.ttf', substr($ans[0][1],1,6));
        imagefttext($im, 22, 0, 270, 584 + 24 + 50, $wordColor1, 'Public/font.ttf', substr($ans[1][1],1,6));
        imagefttext($im, 22, 0, 270, 584 + 24 + 100, $wordColor1, 'Public/font.ttf', substr($ans[2][1],1,6));
        imagefttext($im, 22, 0, 270, 584 + 24 + 150, $wordColor1, 'Public/font.ttf', substr($ans[3][1],1,6));
        imagefttext($im, 22, 0, 270, 584 + 24 + 200, $wordColor1, 'Public/font.ttf', substr($ans[4][1],1,6));

        for($i=0;$i<5;$i++){
            global $mark;
            $mark = 0;
            if($maxv == $ans[$i][0] && $mark==0){
                imagefttext($im, 22, 0, 270+65, 584 + 24 + 50*$i, $colorLine1, 'Public/font.ttf','爆表');
                $mark = 1;
            }else{
                imagefttext($im, 22, 0, 270+65, 584 + 24 + 50*$i, $colorLine1, 'Public/font.ttf',intval(100*$ans[$i][0]/$maxv));
            }
        }

//    输出图像并释放内存
        header('Content-type: image/png');
//        imagepng($im,'boygirlgod/temp.png');
        imagepng($im);
        imagedestroy($im);


    }

    public function sel()
    {
        $DAO = M('');
        $_id = 1;
        $re = $DAO->query("
            select * from boygirlgod_main where id =" . $_id);
        echo var_dump($re);
        echo $re[0]["id"] . ">>" . $re[0]["hero"] . ">>" . $re[0]["ans"];

    }

    public function save2db()
    {
        $_hero = $_POST['_SEX'];
        $_json = $_POST["_JSON"];
        $_maxv = $_POST["_MAXV"];
        $_maxs = $_POST["_MAXS"];

        $DAO = M('');
        $re = $DAO->execute("
            insert into boygirlgod_main (hero, ans, maxv, maxs) values ('" . $_hero . "', '" . $_json . "','" . $_maxv . "','" . $_maxs . "');
            ");
        if ($re) {
            $re = $DAO->query("
                select max(id) as nums from boygirlgod_main;
                ");
        }
        $uid =  $re[0]['nums'];
        echo "[{'uid':$uid}]";

    }

    public function answer(){
        $DAO = M('');
        $re = $DAO->query("
                select max(id) as nums from boygirlgod_main;
                ");
        //当前参与人数
        $this->members = $re[0]['nums'];


        $uid = $_GET["_URL_"][2];
        $ans_img_url= 'http://139.196.27.59/lukas/index.php/load/load/'.$uid;
        $this->imgurl = $ans_img_url;

        $sex = $_GET["_URL_"][3];
        $maxs = $_GET["_URL_"][4];

        if($sex == "boy"){
            $title = "准！我的LOL男神就是这样的!你也来试试吧！";
        }else if($sex == "girl"){
            $title = "准！我的LOL女神就是这样的!你也来试试吧！";
        }else{
            $title = "准！我的LOL男神/女神就是这样的！你来试试吧！";
        }
        $this->title = $title;


        if($sex == 'boy'){
            //男神
            if($maxs == '1颜值'){
                $context = '在别的游戏里，像他这么帅的一般都是主角！你算是捡到宝了！想象一下以后每天早上一睁眼就能看到这样一张脸。是不是已经开始流口水了？';
            }else if($maxs == '2财力'){
                $context = '嘉文四世，无疑是德玛西亚最富有的富二代。他的财力，权利都无可匹敌，更没人敢违抗他的意志。而你就是唯一一个让他不知所措的人。';
            }else if($maxs == '3霸道'){
                $context = '“生命中有三件必经之事：荣誉、死亡、还有宿醉。”这是他常挂于嘴的话。但那是他在遇见你之前的事情了。当他遇到你时，一切都不再重要了。';
            }else if($maxs == '4高冷'){
                $context = '杀得了师父，盗得了暗黑奥义，看起来心狠手辣的他，其实内心却住着一个少年。而这个少年是你发现的。';
            }else if($maxs == '5体贴'){
                $context = '全身心保护皮城人民的杰斯，是皮尔特沃夫通向光明未来的希望。这样一个背负重担的他却遇到了如水般的你，他只为你放下所有高大威猛的形象。';
            }else if($maxs == '6傲娇'){
                $context = '作为德玛西亚之力，他以为他可以把整个世界都给举起来，而他却不知道如何得到你的芳心。还拼命想要掩饰自己已经坠入爱河的内心。殊不知他的这种可爱面早已经打动了你。';
            }else if($maxs == '7呆萌'){
                $context = '体型并不能说明一切……可是无疑正是他的这种可爱又呆萌的样子深深吸引着你。他好动，喜欢恶作剧，但这些都只是他发自内心的天然呆的表现。你对他无可奈何，却又喜欢得不得了。所以，你就认命吧。';
            }else if($maxs == '8逗比'){
                $context = '五秒真男人！这是大家赋予蛮三刀的逗比美誉。可是为什么呢？当他遇到你时，他想摆脱这个逗比的形象。他主动追求你，却逗比百出……但是who care！你就是喜欢这样逗比的蛮三刀！';
            }else{
                $context = "";
            }
        }else{
            //女神
            if($maxs == '1颜值'){
                $context = '当无数人拜倒在阿狸的魅惑诱术之下时，只有你透过她无可救药的颜值看到了她善良的内心。没错，她将爱上这样一个特别的你。';
            }else if($maxs == '2财力'){
                $context = '不要怀疑你的运气，小朋友！能约到卡特琳娜这个大土豪真是太幸福了，快想想有钱人的生活别提多带感了！';
            }else if($maxs == '3霸道'){
                $context = '现在是大搜查时间，凯特琳不喜欢的人的全被她抓起来了，而你就是那个陪伴在她身边的幸运儿！';
            }else if($maxs == '4高冷'){
                $context = '预测的结果非常清楚，只要998迦娜就能让你爽到不能呼吸哟，还在等什么快去约了她！';
            }else if($maxs == '5体贴'){
                $context = '文艺体贴的娑娜，上得厅堂下得厨房娶回家一定是个贤妻良母，你是不是心动了呢，而她就是你命中注定的另一半。';
            }else if($maxs == '6傲娇'){
                $context = '哼笨蛋！我才不会陪你去约会呢，人家才不会这么轻浮呢，不过你求我的话我也不是不能答应哦，嘿嘿！';
            }else if($maxs == '7呆萌'){
                $context = '如此可爱的奥莉安娜，生活中却笨手笨脚。是不是很想照顾她，保护她呢？别犹豫了，只有你可以成为她的白马王子。';
            }else if($maxs == '8逗比'){
                $context = '你这个小松鼠很眼熟啊，原来是我变得！小松鼠你要好好陪着我啊，我们要一辈子在一起，嘻嘻还是把你变回来吧!';
            }else{
                $context = "";
            }
        }
        $this->context = $context ;

        $this->display();
    }

    //废弃
    public function share(){
        $DAO = M('');
        $re = $DAO->query("
                select max(id) as nums from boygirlgod_main;
                ");
        //当前参与人数
        $this->members = $re[0]['nums'];

        $name = "Ta";

        $uid = $_GET["_URL_"][2];
        $ans_img_url= 'http://139.196.27.59/lukas/index.php/load/load/'.$uid;
        $this->imgurl = $ans_img_url;
        $this->name= $name;
        $this->display();
    }

    public function submit(){
        $this->display();
    }

    public function aboutus(){
        $this->display();
    }

    public function start(){
        $this->display();
    }



}