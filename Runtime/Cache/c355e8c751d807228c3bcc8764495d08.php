<?php if (!defined('THINK_PATH')) exit();?><!DOCTYPE html>
<html>
<head lang="ch">
    <meta charset="UTF-8">
    <title>我的男神/女神</title>
    <meta name="viewport" content="width=device-width"/>

    <link rel="stylesheet" href="http://code.jquery.com/mobile/1.3.2/jquery.mobile-1.3.2.min.css">
    <script src="http://libs.baidu.com/jquery/1.9.0/jquery.js"></script>
    <script src="http://libs.baidu.com/jquerymobile/1.3.0/jquery.mobile-1.3.0.js"></script>
    <!--<link rel="stylesheet" type='text/css' href="Public/jscss/jquery.mobile-1.3.2.css">-->
    <!--<script src="Public/jscss/jquery-1.8.3.js "></script>-->
    <!--<script src="Public/jscss/jquery.mobile-1.3.2.js "></script>-->

    <!--<link rel="stylesheet" href="http://code.jquery.com/mobile/1.3.2/jquery.mobile-1.3.2.min.css">-->
    <!--<script src="http://code.jquery.com/jquery-1.8.3.min.js"></script>-->
    <!--<script src="http://code.jquery.com/mobile/1.3.2/jquery.mobile-1.3.2.min.js"></script>-->


</head>
<body>

<div data-role="page" id="page0">
    <div data-role="header">
        <h1>我的男神女神</h1>
    </div>
    <fieldset data-role="controlgroup">
        <legend>你要男神还是女神？</legend>
        <input type="radio" name="radio-choice" id="q0a" value="choice-1" checked="checked"/>
        <label for="q0a">我喜欢男人</label>

        <input type="radio" name="radio-choice" id="q0b" value="choice-2"/>
        <label for="q0b">我喜欢女人</label>
    </fieldset>
    <div data-role="content">
        <a href="#page1" data-transition="slide" onclick="q0();">下一题</a>
    </div>
</div>


<div data-role="page" id="page1">
    <div data-role="header">
        <h1>我的男神女神</h1>
    </div>
    <fieldset data-role="controlgroup">
        <legend>你曾经希望自己变成异性吗？</legend>
        <input type="radio" name="radio-choice" id="q1a" value="choice-1" checked="checked"/>
        <label for="q1a">有，并且想有朝一日一定要变性</label>

        <input type="radio" name="radio-choice" id="q1b" value="choice-2"/>
        <label for="q1b">没有，并且反对变性</label>

        <input type="radio" name="radio-choice" id="q1c" value="choice-3"/>
        <label for="q1c">有过，但是现在这样也挺好</label>

        <input type="radio" name="radio-choice" id="q1d" value="choice-4"/>
        <label for="q1d">没有，但是也不反对别人变性</label>
    </fieldset>
    <div data-role="content">
        <a href="#page2" data-transition="slide" onclick="q1();">下一题</a>
    </div>
</div>

<div data-role="page" id="page2">
    <div data-role="header">
        <h1>我的男神女神</h1>
    </div>
    <fieldset data-role="controlgroup">
        <legend>路上你遇见了你喜欢的Ta会怎么样？</legend>
        <input type="radio" name="radio-choice" id="q2a" value="choice-1" checked="checked"/>
        <label for="q2a">主动跟他打招呼</label>

        <input type="radio" name="radio-choice" id="q2b" value="choice-2"/>
        <label for="q2b">默默地跟在他后面</label>

        <input type="radio" name="radio-choice" id="q2c" value="choice-3"/>
        <label for="q2c">加速走到他前面</label>

        <input type="radio" name="radio-choice" id="q2d" value="choice-4"/>
        <label for="q2d">什么都不做</label>
    </fieldset>
    <div data-role="content">
        <a href="#page3" data-transition="slide" onclick="q2();">下一题</a>
    </div>
</div>


<div data-role="page" id="page3">
    <div data-role="header">
        <h1>我的男神女神</h1>
    </div>
    <fieldset data-role="controlgroup">
        <legend>讲真话，你过生日希望收到怎样的生日礼物</legend>
        <input type="radio" name="radio-choice" id="q3a" value="choice-1" checked="checked"/>
        <label for="q3a">名贵奢侈品买买买</label>

        <input type="radio" name="radio-choice" id="q3b" value="choice-2"/>
        <label for="q3b">送啥都行来者不拒</label>

        <input type="radio" name="radio-choice" id="q3c" value="choice-3"/>
        <label for="q3c">必须按我的要求买礼物</label>

        <input type="radio" name="radio-choice" id="q3d" value="choice-4"/>
        <label for="q3d">只接受我喜欢的人的礼物</label>
    </fieldset>
    <div data-role="content">
        <a href="#page4" data-transition="slide" onclick="q3();">下一题</a>
    </div>
</div>

<div data-role="page" id="page4">
    <div data-role="header">
        <h1>我的男神女神</h1>
    </div>
    <fieldset data-role="controlgroup">
        <legend>你约喜欢的异性出去玩，会选择去哪里？</legend>
        <input type="radio" name="radio-choice" id="q4a" value="choice-1" checked="checked"/>
        <label for="q4a">电影院</label>

        <input type="radio" name="radio-choice" id="q4b" value="choice-2"/>
        <label for="q4b">游乐场</label>

        <input type="radio" name="radio-choice" id="q4c" value="choice-3"/>
        <label for="q4c">高档购物中心</label>

        <input type="radio" name="radio-choice" id="q4d" value="choice-4"/>
        <label for="q4d">动物园</label>
    </fieldset>
    <div data-role="content">
        <a href="#page5" data-transition="slide" onclick="q4();">下一题</a>
    </div>
</div>

<div data-role="page" id="page5">
    <div data-role="header">
        <h1>我的男神女神</h1>
    </div>
    <fieldset data-role="controlgroup">
        <legend>有一天，你喜欢的异性找你倾诉Ta暗恋别人的烦恼，你会？</legend>
        <input type="radio" name="radio-choice" id="q5a" value="choice-1" checked="checked"/>
        <label for="q5a">说“关我屁事啊”</label>

        <input type="radio" name="radio-choice" id="q5b" value="choice-2"/>
        <label for="q5b">倾听并安慰Ta</label>

        <input type="radio" name="radio-choice" id="q5c" value="choice-3"/>
        <label for="q5c">乘机表白</label>

        <input type="radio" name="radio-choice" id="q5d" value="choice-4"/>
        <label for="q5d">带他去high，转移注意力</label>
    </fieldset>
    <div data-role="content">
        <a href="#page6" data-transition="slide" onclick="q5();">下一题</a>
    </div>
</div>

<div data-role="page" id="page6">
    <div data-role="header">
        <h1>我的男神女神</h1>
    </div>
    <fieldset data-role="controlgroup">
        <legend>路上遇到何种倒霉事，能让你接受？</legend>
        <input type="radio" name="radio-choice" id="q6a" value="choice-1" checked="checked"/>
        <label for="q6a">被捕鼠器夹到</label>

        <input type="radio" name="radio-choice" id="q6b" value="choice-2"/>
        <label for="q6b">被牛顶到</label>

        <input type="radio" name="radio-choice" id="q6c" value="choice-3"/>
        <label for="q6c">掉进河里居然都能遇到鲸鱼</label>

    </fieldset>
    <div data-role="content">
        <a href="#pageans" data-transition="slide" onclick="q6();">去看看结果吧-> </a>
    </div>
</div>

<div data-role="page" id="pageans">
    <div data-role="header">
        <h1>我的男神女神</h1>
    </div>

    <div id="container" style="min-width:700px;height:400px"></div>
    ﻿
    <div id="sliders" style="min-width:310px;max-width: 800px;margin: 0 auto;">

    </div>

</div>


<script type="text/javascript">
    var sex;
    function q0() {
        if (document.getElementById("q0a").checked) {
            sex = 'boy';
        } else if (document.getElementById("q0b").checked) {
            sex = 'girl';
        }
        console.log("sex", sex);
    }

    var arr = new Array();
    var arrFinal = new Array();
    function q1() {
        if (document.getElementById("q1a").checked) {
            arr[1] = [0, 0, 0, 0, 9, 0, 5, 5];
        } else if (document.getElementById("q1b").checked) {
            arr[1] = [5, 10, 0, 0, 0, 0, 0, 0];
        } else if (document.getElementById("q1c").checked) {
            arr[1] = [0, 0, 10, 0, 0, 0, 0, 0];
        } else if (document.getElementById("q1d").checked) {
            arr[1] = [0, 0, 0, 10, 0, 5, 0, 0];
        }
        console.log("arr[1]>>>", arr[1]);
    }

    function q2() {
        if (document.getElementById("q2a").checked) {
            arr[2] = [10, 0, 5, 0, 0, 0, 0, 0];
        } else if (document.getElementById("q2b").checked) {
            arr[2] = [5, 0, 0, 6, 7, 0, 0, 0];
        } else if (document.getElementById("q2c").checked) {
            arr[2] = [8, 0, 10, 0, 0, 7, 0, 0];
        } else if (document.getElementById("q2d").checked) {
            arr[2] = [0, 0, 0, 10, 0, 10, 0, 0];
        }
        console.log("arr[2]>>>", arr[2]);
    }

    function q3() {
        if (document.getElementById("q3a").checked) {
            arr[3] = [0, 20, 2, 0, 0, 0, 0, 0];
        } else if (document.getElementById("q3b").checked) {
            arr[3] = [0, 2, 8, 10, 0, 0, 0, 0];
        } else if (document.getElementById("q3c").checked) {
            arr[3] = [0, 0, 0, 0, 9, 0, 0, 0];
        } else if (document.getElementById("q3d").checked) {
            arr[3] = [10, 0, 0, 0, 0, 0, 0, 0];
        }
        console.log("arr[3]>>>", arr[3]);
    }

    function q4() {
        if (document.getElementById("q4a").checked) {
            arr[4] = [0, 0, 0, 0, 0, 10, 0, 0];
        } else if (document.getElementById("q4b").checked) {
            arr[4] = [0, 0, 0, 0, 0, 0, 0, 10];
        } else if (document.getElementById("q4c").checked) {
            arr[4] = [0, 10, 0, 0, 0, 0, 0, 0];
        } else if (document.getElementById("q4d").checked) {
            arr[4] = [0, 0, 0, 0, 0, 0, 10, 0];
        }
        console.log("arr[4]>>>", arr[4]);
    }

    function q5() {
        if (document.getElementById("q5a").checked) {
            arr[5] = [0, 0, 0, 8, 0, 5, 0, 0];
        } else if (document.getElementById("q5b").checked) {
            arr[5] = [0, 0, 0, 0, 9, 0, 0, 0];
        } else if (document.getElementById("q5c").checked) {
            arr[5] = [5, 0, 5, 0, 0, 0, 0, 0];
        } else if (document.getElementById("q5d").checked) {
            arr[5] = [0, 0, 0, 0, 0, 0, 7, 10];
        }
        console.log("arr[5]>>>", arr[5]);
    }

    function q6() {
        if (document.getElementById("q6a").checked) {
            arr[6] = [0, 0, 0, 0, 9, 0, 0, 0];
        } else if (document.getElementById("q6b").checked) {
            arr[6] = [0, 0, 0, 0, 0, 0, 10, 5];
        }else if(document.getElementById("q6c").checked){
            arr[6] =[0, 0, 0, 0, 0, 0, 5, 10];
        }
        console.log("arr[6]>>>", arr[6]);

        getFinal()
    }


    function getFinal() {
        arrFinal = [0, 0, 0, 0, 0, 0, 0, 0];
        for (var i = 1; i <= 6; i++) {
            for (var j = 0; j < 8; j++) {
                arrFinal[j] += arr[i][j];
            }
        }
        console.log('arrFinal>>>', arrFinal);


        xcoordinate = ['1颜值', '2财力','3霸道','4高冷', '5体贴', '6傲娇', '7呆萌', '8逗比']
////        组装新list用来排序
//        arrObj = new Array();
//        for (var i = 0; i<8;i++){
//            arrObj.push([arrFinal[i],xcoordinate[i]])
//        }
//        arrObj.sort(function(a,b){return b[0]-a[0];})
//        console.log('arrObj>>>', arrObj);
//
////        var timestamp = new Date().getTime();
//
//        var jsondata = JSON.stringify(arrObj);
//        console.log('jsondata>>>', jsondata);
//        $.ajax({
//            type: 'POST',
//            url: "<?php echo U('Load/load');?>",
//            data: {_JSON:jsondata},
//            dataType: 'json'
//        });
//
////        des2show = new Array();
////        v2show = new Array();
//////        只显示前五个
////        for (var i = 0 ;i <5 ;i++){
////            des2show.push(arrObj[i][1]);
////            v2show.push(arrObj[i][0]);
////        }
////        console.log('des2show>>>', des2show);
////        console.log('v2show>>>', v2show);
        //        组装新list用来排序
        arrObj = new Array();
        for (var i = 0; i<8;i++){
            arrObj.push([arrFinal[i],xcoordinate[i]])
        }
        //根据值降序
        arrObj.sort(function(a,b){return b[0]-a[0];})
        //剔除后三位
        arrObj.pop();arrObj.pop();arrObj.pop();
        var maxv = arrObj[0][0];
        var maxs = arrObj[0][1];
        console.log('maxv>>>', maxv);
        console.log('maxs>>>', maxs);
        //根据需求再升序
        arrObj.sort(function(a,b){return a[1].charAt(0)-b[1].charAt(0)});
        console.log('arrObj>>>', arrObj);

        var jsondata = JSON.stringify(arrObj);
        console.log('jsondata>>>', jsondata);
        $.ajax({
            type: 'POST',
            url: "<?php echo U('Load/save2db');?>",
            data: {_JSON:jsondata,_SEX:sex,_MAXV:maxv,_MAXS:maxs},
            dataType: 'json',
            complete: function(ans){
                var ans = eval(ans.responseText);
                var uid = ans[0]['uid'];
                console.log("UID::",uid);

                //go answer
                window.location.href="http://localhost/lukas/index/load/load/"+uid;
            }
        });

    }

</script>
</body>
</html>